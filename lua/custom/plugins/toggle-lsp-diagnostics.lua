return {
    "WhoIsSethDaniel/toggle-lsp-diagnostics.nvim",
    event = { "BufReadPost", "BufNewFile" },
    config = function()
      require("toggle_lsp_diagnostics").init(vim.diagnostic.config())
    end,
}
