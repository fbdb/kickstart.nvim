return {
 'akinsho/toggleterm.nvim',
 version = "*",
 opts = {
  size = 10,
  direction = "float",
  open_mapping = [[<F7>]],
 }
}
